//! Before any reads or writes are performed we check the validity of the
//! destination/source IDs. An ID is valid if it's between 0 and 31. For any
//! number larger than 31 we return 0.
//!
//! # Example:
//!
//! ```
//! # use adept_lib::register_file::RegisterFile;
//! # use adept_lib::riscv::decoder::Instruction;
//! let mut my_reg_file = RegisterFile::new();
//! let decode=Instruction::new(0b0000000_00000_00000_000_00000_0110011);
//! // Write 5 to register 0 is ignored
//! my_reg_file.write(&decode,5);
//! // However a write to 21 is valid
//! let decode=Instruction::new(0b0000000_10101_00000_000_10101_0110011);
//! my_reg_file.write(&decode, 31);
//! assert_eq!((0, 31), my_reg_file.read(&decode));
//! ```

use riscv::decoder::Instruction;
use std::fmt::{self, Display, Formatter};

#[derive(Default)]
pub struct RegisterFile {
    registers: Vec<i32>,
}

impl RegisterFile {
    pub fn new() -> Self {
        RegisterFile {
            // Create 31 registers, register 0 is always 0
            registers: vec![0; 31],
        }
    }

    /// Write data to a register given an ID
    ///
    /// Before writing the data the ID is checked. If it's register 0 or id is
    /// greater than 31, ignore write.
    ///
    /// # Arguments
    /// * `decoder` => Instruction that contains the identification
    /// number of the Register to write data on
    /// * `data` => the data to be stored
    pub fn write(&mut self, decoder: &Instruction, data: i32) {
        if let Some(rsd) = decoder.get_rd() {
            if rsd != 0 && rsd < 32 {
                self.registers[rsd as usize - 1] = data;
            }
        }
    }

    /// Read the contents of two registers simultaneously
    ///
    /// Before reading the data the ID is checked. If it's register 0 or id is
    /// greater than 31, return 0.
    ///
    /// # Arguments
    /// * `decoder` => Instruction that contains the identification
    /// numbers of Registers to read from
    pub fn read(&self, decoder: &Instruction) -> (i32, i32) {
        let rs1_read = if decoder.get_rs1().is_some() {
            if decoder.get_rs1().unwrap() == 0 || decoder.get_rs1().unwrap() >= 32 {
                0
            } else {
                self.registers[decoder.get_rs1().unwrap() as usize - 1]
            }
        } else {
            0
        };
        let rs2_read = if decoder.get_rs2().is_some() {
            if decoder.get_rs2().unwrap() == 0 || decoder.get_rs2().unwrap() >= 32 {
                0
            } else {
                self.registers[decoder.get_rs2().unwrap() as usize - 1]
            }
        } else {
            0
        };

        (rs1_read, rs2_read)
    }
}

impl Display for RegisterFile {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        writeln!(f, "R{0:<2}:0x{0:08x}", 0)?;
        for i in 1..32 {
            writeln!(f, "R{0:<2}:0x{1:08x}", i, self.registers[i as usize - 1])?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_rw() {
        let mut reg_file = RegisterFile::new();
        // RD=8
        let decode = Instruction::new(0b0000000_00000_00000_000_00100_0110011);
        reg_file.write(&decode, 54); // write to Reg 8
                                     // RD=16. RS2=8, RS1=16
        let decode = Instruction::new(0b0000000_00100_01000_000_01000_0110011);
        reg_file.write(&decode, 74); // write to Reg 16
                                     // Read Reg 8 and 16 to heck if writes were valid
        assert_eq!((74, 54), reg_file.read(&decode));
    }

    #[test]
    fn test_rw_to_r0() {
        let mut reg_file = RegisterFile::new();
        // RS2=1 RS1=0 RD=0
        let decode = Instruction::new(0b0000000_00001_00000_000_00000_0110011);
        // Write to Reg 0 twice
        reg_file.write(&decode, 54);
        reg_file.write(&decode, 74);
        // Check Reg 0 value
        assert_eq!((0, 0), reg_file.read(&decode));
    }
}
